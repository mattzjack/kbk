use std::{
    env,
    fs,
    fs::File,
};
use time::format_description::well_known::{iso8601, Iso8601};

enum Command {
    Hilfe,
    Neu,
    Zeigen,
    Loeschen,
}

// TODO: move this into config
const KALENDER_PATH: &str = "/home/kyz/Desktop/kalender.tsv";

// serde stuff
const ISO_DATE_CONFIG: iso8601::EncodedConfig = iso8601::Config::DEFAULT.set_formatted_components(iso8601::FormattedComponents::Date).encode();
const ISO_DATE_FORMAT: Iso8601<ISO_DATE_CONFIG> = Iso8601::<ISO_DATE_CONFIG>;
time::serde::format_description!(iso_date, Date, ISO_DATE_FORMAT);

#[derive(Debug, serde::Deserialize, serde::Serialize)]
struct Ereignis {
    #[serde(with = "iso_date")]
    gregorianischer: time::Date,
    beschreibung: String,
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let cmd = get_cmd(&args);

    match cmd {
        Command::Hilfe    => return hilfe(),
        Command::Loeschen => return loeschen(&args),
        Command::Neu      => return neu(&args),
        Command::Zeigen   => return zeigen(),
    }
}

fn get_cmd(args: &[String]) -> Command {
    if args.len() < 2 {
        return Command::Hilfe
    }
    let cmd = &args[1].to_lowercase();
    if cmd == "z" || cmd == "zeig" {
        return Command::Zeigen
    }
    if cmd == "n" || cmd == "neu" {
        return Command::Neu
    }
    if cmd == "l" || cmd == "loesch" {
        return Command::Loeschen
    }
    Command::Hilfe
}

fn get_kalender() -> Vec<Ereignis> {
    // read current calendar
    let kalender_file = File::open(KALENDER_PATH).expect("should be able to open kalender file");
    let mut reader = csv::ReaderBuilder::new().delimiter(b'\t').from_reader(kalender_file);
    reader.deserialize().map(|x| x.expect("should be able to deserialize")).collect()
}

fn save_kalender(k: Vec<Ereignis>) {
    let mut writer = csv::WriterBuilder::new().delimiter(b'\t').from_path(KALENDER_PATH).expect("should be able to make writer");
    for rec in k {
        writer.serialize(rec).expect("should be able to serialize");
    }
    writer.flush().expect("should be able to flush");
}

fn zeigen() {
	let kalender = fs::read_to_string(KALENDER_PATH).expect("should be able to read kalender file");
	println!("{}", kalender)
}

fn hilfe() {
	println!("\
h, hilfe       => Hilfe sehen
l, loeschen i  => das i. Ereignis loeschen
n, neu <g> <b> => neues Ereignis hinzufuegen
                      am Tag <g>, mit der Beschreibung <b>
z, zeig        => Kalender zeigen")
}

fn neu(args: &[String]) {
    // read input
    if args.len() < 4 {
        return hilfe()
    }
    let gregorianischer = time::Date::parse(&args[2], &Iso8601::DATE).expect("should be able to parse date");
    let beschreibung = &args[3];

    let mut records = get_kalender();

    // TODO: refactor to find index to insert
    let mut inserted = false;
    for (i, record) in records.iter().enumerate() {
        if gregorianischer < record.gregorianischer {
            records.insert(i, Ereignis { gregorianischer, beschreibung: beschreibung.to_string() });
            inserted = true;
            break;
        }
    }
    if !inserted {
        records.push(Ereignis { gregorianischer, beschreibung: beschreibung.to_string() });
    }
    // write result out
    save_kalender(records);



    // show result
    zeigen()    
}

fn loeschen(args: &[String]) {
    if args.len() < 3 {
        return hilfe()
    }
    let mut i = args[2].parse::<usize>().expect("should pass a number");
    if i <= 0 {
        return println!("start with 1")
    }
    i -= 1;

    let mut records = get_kalender();

    if i >= records.len() {
        return println!("out of range")
    }

    records.remove(i);
    save_kalender(records);
    zeigen()
}
